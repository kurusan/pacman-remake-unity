# A Unity Project : Pacman Remake (2D)

## Author
> Made by
> * **Mohamed Leye**
> * Senegal, Dakar, ESP, M2 JID

## Description
The project is a Unity game written on c#. The game itself is based on **'Pac-Man'** which has been around since the earliest days of computing.

## Concept

> ### Control PacMan and Menu
> In the menu use **return** to access game as 1 player or 2 players 
> To move Pac-Man, use **up arrow** for up, **down arrow** for down, **left arrow** and **right arrow** for right.

> ### Aim
> The player navigates Pac-Man through a maze containing dots, known as Pac-Dots, and four multi-colored ghosts: Blinky, Pinky, Inky and Clyde. 
> The goal of the game is to accumulate as many points as possible by collecting the dots and eating ghosts.
> PacMan has 3 lives when you lose all lives the **Game Over**.
> Near the corners of the maze are four flashing Power Pellets that provide Pac-Man with the temporary ability to eat the ghosts and earn bonus points. The enemies turn deep blue, reverse direction and usually move more slowly. 
> When an enemy is eaten, its eyes remain and return to the center box where the ghost is regenerated in its normal color.

## Runnable file
You can download the executable file [here](https://drive.google.com/file/d/14a4ylOOFwzrlVyB24uAI94lPi_LTvoZC/view?usp=sharing).




