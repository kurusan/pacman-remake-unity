﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameMenu : MonoBehaviour
{
    public static bool _isOnePlayerGame = true;


    public Text _playerText1;
    public Text _playerText2;
    public Text _playerSelector;


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            if (!_isOnePlayerGame)
            {
                _isOnePlayerGame = true;
                _playerSelector.transform.localPosition = new Vector3(_playerSelector.transform.localPosition.x, _playerText1.transform.localPosition.y, _playerSelector.transform.localPosition.z);
            }
        }
        else if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            if (_isOnePlayerGame)
            {
                _isOnePlayerGame = false;
                _playerSelector.transform.localPosition = new Vector3(_playerSelector.transform.localPosition.x, _playerText2.transform.localPosition.y, _playerSelector.transform.localPosition.z);
            }
        }
        else if (Input.GetKeyUp(KeyCode.Return))
        {
            SceneManager.LoadScene("Level1");
        }
    }
}
