﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ghost : MonoBehaviour
{
    public enum Mode
    {
        CHASE,
        SCATTER,
        FRIGHTENED,
        CONSUMED
    }

    Mode _currentMode = Mode.SCATTER;
    Mode _previousMode;

    public enum GhostType
    {
        RED,
        PINK,
        BLUE,
        ORANGE
    }

    public GhostType ghostType = GhostType.RED;

    public float _moveSpeed = 5.9f;
    public float _normalMoveSpeed = 5.9f;
    public float _frightenedMoveSpeed = 2.9f;
    public float _consumedMoveSpeed = 15f;

    //-- Pink Ghost release timer
    public int _pinkyReleaseTimer = 5;
    //-- Blue Ghost release timer
    public int _inkyReleaseTimer = 14;
    //-- Orange Ghost release timer
    public int _clydeReleaseTimer = 21;
    public float _ghostReleaseTimer = 0;

    public int _frightenedModeDuration = 10;
    public int _startBlinkyAt = 7;

    public bool _isInGhostHouse = false;
    public bool _canMove = true;

    public int _scatterModeTimer1 = 7;
    public int _chaseModeTimer1 = 20;
    public int _scatterModeTimer2 = 7;
    public int _chaseModeTimer2 = 20;
    public int _scatterModeTimer3 = 5;
    public int _chaseModeTimer3 = 20;
    public int _scatterModeTimer4 = 5;

    public Sprite _eyesLeft;
    public Sprite _eyesRight;
    public Sprite _eyesUp;
    public Sprite _eyesDown;

    public RuntimeAnimatorController _ghostLeft;
    public RuntimeAnimatorController _ghostRight;
    public RuntimeAnimatorController _ghostUp;
    public RuntimeAnimatorController _ghostDown;
    public RuntimeAnimatorController _ghostScared;
    public RuntimeAnimatorController _ghostWhite;

    public Node _startingPosition;
    public Node _homeNode;
    public Node _ghostHouse;

    private int _modeChangeIteration = 1;
    private float _modeChangeTimer = 0;

    private float _previousMoveSpeed;

    private float _frightenedModeTimer = 0;
    private float _blinkTimer = 0;

    private bool _frightenedModeIsWhite= false;

    private Node _currentNode;
    private Node _targetNode;
    private Node _previousNode;
    private Vector2 _direction, _nexDirection;

    private AudioSource _backgroundAudio;

    private GameObject _pacman;

    // Start is called before the first frame update
    void Start()
    {
        _backgroundAudio = GetGameObject.Game.GetComponent<AudioSource>();

        _pacman = GetGameObject.PacmanByTag;
        Node node = GetNode.NodeAtPosition(transform.localPosition);

        if (node != null)
        {
            _currentNode = node;
        }

        if(_isInGhostHouse)
        {
            _direction = Vector2.up;
            _targetNode = _currentNode._neighbors[0];
        }
        else
        {
            _direction = Vector2.left;
            _targetNode = ChooseNextNode();
        }
        _previousNode = _currentNode;

        UpdateAnimatorController();
    }

    public void MoveToStartingPosition ()
    {
        if (transform.name != "Blinky")
        {
            _isInGhostHouse = true;
        }

        transform.position = _startingPosition.transform.position;

        if (_isInGhostHouse)
        {
            _direction = Vector2.up;
        }
        else
        {
            _direction = Vector2.left;
        }

        UpdateAnimatorController();
    }

    //-- Reinitialize ghots properties to there default values when player lose
    public void Restart()
    {
        _canMove = true;

        _currentMode = Mode.SCATTER;

        _moveSpeed = _normalMoveSpeed;
        _previousMoveSpeed = 0;


        _ghostReleaseTimer = 0;
        _modeChangeIteration = 1;
        _modeChangeTimer = 0;
      
        _currentNode = _startingPosition;

        if (_isInGhostHouse)
        {
            _direction = Vector2.up;
            _targetNode = _currentNode._neighbors[0];
        }
        else
        {
            _direction = Vector2.left;
            _targetNode = ChooseNextNode();
        }
        _previousNode = _currentNode;
        UpdateAnimatorController();
    }

    // Update is called once per frame
    void Update()
    {
        if (_canMove)
        {
            ModeUpdate();
            Move();
            ReleaseGhosts();
            CheckColission();
            CheckIsInGhostMode();
        }
    }

    void CheckIsInGhostMode()
    {
        if (_currentMode == Mode.CONSUMED)
        {
            GameObject tile = GetGameObject.TileAtPosition(transform.position);
            if(tile != null)
            {
                if (tile.transform.GetComponent<Tile>() != null)
                {
                    if (tile.transform.GetComponent<Tile>()._isGhostHouse)
                    {
                        _moveSpeed = _normalMoveSpeed;
                        Node node = GetNode.NodeAtPosition(transform.position);
                        if (node != null)
                        {
                            _currentNode = node;
                            _direction = Vector2.up;
                            _targetNode = _currentNode._neighbors[0];
                            _previousNode = _currentNode;
                            _currentMode = Mode.CHASE;

                            UpdateAnimatorController();
                        }
                    }
                }
            }
        }
    }

    void CheckColission()
    {
        Rect ghostRect = new Rect(transform.position, transform.GetComponent<SpriteRenderer>().sprite.bounds.size / 4);
        Rect pacmanRect = new Rect(_pacman.transform.position, transform.GetComponent<SpriteRenderer>().sprite.bounds.size / 4);

        if (ghostRect.Overlaps(pacmanRect))
        {
            if(_currentMode == Mode.FRIGHTENED)
            {
                Consumed();
            }
            else 
            {
                //-- Pacman should die here
                //-- Only if the mode is different to consumed
                if (_currentMode != Mode.CONSUMED)
                {
                    GetGameObject.Game.transform.GetComponent<GameBoard>().StartDeath();
                }
            }
        }
    }

    void Consumed()
    {

        if (GameMenu._isOnePlayerGame)
        {
            GetGameObject.Game.GetComponent<GameBoard>()._playerOneScore += 200;
        }
        else
        {
            if (GetGameObject.Game.GetComponent<GameBoard>()._isPlayerOneUp)
            {
                GetGameObject.Game.GetComponent<GameBoard>()._playerOneScore += 200;
            }
            else
            {
                GetGameObject.Game.GetComponent<GameBoard>()._playerTwoScore += 200;
            }
        }

        _currentMode = Mode.CONSUMED;
        _previousMoveSpeed = _moveSpeed;
        _moveSpeed = _consumedMoveSpeed;

        UpdateAnimatorController();

        GetGameObject.Game.transform.GetComponent<GameBoard>().StartConsumed(this.GetComponent<Ghost>());
    }

    void UpdateAnimatorController()
    {
        if (_currentMode != Mode.FRIGHTENED && _currentMode != Mode.CONSUMED )
        {
            // If ghost go to left
            if (_direction == Vector2.left)
            {
                transform.GetComponent<Animator>().runtimeAnimatorController = _ghostLeft;
            }
            // If ghost go to right
            else if (_direction == Vector2.right)
            {
                transform.GetComponent<Animator>().runtimeAnimatorController = _ghostRight;
            }
            // If ghost go to up
            else if (_direction == Vector2.up)
            {
                transform.GetComponent<Animator>().runtimeAnimatorController = _ghostUp;
            }
            // If ghost go to down
            else if (_direction == Vector2.down)
            {
                transform.GetComponent<Animator>().runtimeAnimatorController = _ghostDown;
            }
            else
            {
                transform.GetComponent<Animator>().runtimeAnimatorController = _ghostLeft;
            }
        }
        else if (_currentMode == Mode.FRIGHTENED)
        {
            transform.GetComponent<Animator>().runtimeAnimatorController = _ghostScared;
        }
        else if (_currentMode == Mode.CONSUMED)
        {
            transform.GetComponent<Animator>().runtimeAnimatorController = null;
            if (_direction == Vector2.left)
            {
                transform.GetComponent<SpriteRenderer>().sprite = _eyesLeft;
            }
            else if (_direction == Vector2.right)
            {
                transform.GetComponent<SpriteRenderer>().sprite = _eyesRight;
            }
            else if (_direction == Vector2.up)
            {
                transform.GetComponent<SpriteRenderer>().sprite = _eyesUp;
            }
            else if (_direction == Vector2.down)
            {
                transform.GetComponent<SpriteRenderer>().sprite = _eyesDown;
            }
        }
    }

    void Move()
    {
        if (_targetNode != _currentNode && _targetNode != null && !_isInGhostHouse)
        {
            if (Tools.OverShotTarget(transform.position, _targetNode, _previousNode))
            {
                _currentNode = _targetNode;
                transform.localPosition = _currentNode.transform.position;

                GameObject otherPortal = GetGameObject.Portal(_currentNode.transform.position);
                if (otherPortal != null)
                {
                    transform.localPosition = otherPortal.transform.position;
                    _currentNode = otherPortal.GetComponent<Node>();
                }

                _targetNode = ChooseNextNode() ;
                _previousNode = _currentNode;
                _currentNode = null;

                //-- Update Animation when direction is changed
                UpdateAnimatorController();
            }
            else
            {
                //-- Move Ghost
                transform.localPosition += (Vector3)(_direction * _moveSpeed) * Time.deltaTime;
            }
        }
    }


    //-- Update Ghost movements according to there Mode status
    void ModeUpdate()
    {
        
        if(_currentMode != Mode.FRIGHTENED)
        {
            _modeChangeTimer += Time.deltaTime;
            if(_modeChangeIteration == 1)
            {
                if(_currentMode == Mode.SCATTER && _modeChangeTimer > _scatterModeTimer1)
                {
                    ChangeMode(Mode.CHASE);
                    _modeChangeTimer = 0;
                }
                if(_currentMode == Mode.CHASE && _modeChangeTimer > _chaseModeTimer1)
                {
                    _modeChangeIteration = 2;
                    ChangeMode(Mode.SCATTER);
                    _modeChangeTimer = 0;
                }
            }
            else if (_modeChangeIteration == 2)
            {
                if (_currentMode == Mode.SCATTER && _modeChangeTimer > _scatterModeTimer2)
                {
                    ChangeMode(Mode.CHASE);
                    _modeChangeTimer = 0;
                }
                if (_currentMode == Mode.CHASE && _modeChangeTimer > _chaseModeTimer2)
                {
                    _modeChangeIteration = 3;
                    ChangeMode(Mode.SCATTER);
                    _modeChangeTimer = 0;
                }
            }
            else if (_modeChangeIteration == 3){
                if (_currentMode == Mode.SCATTER && _modeChangeTimer > _scatterModeTimer3)
                {
                    ChangeMode(Mode.CHASE);
                    _modeChangeTimer = 0;
                }
                if (_currentMode == Mode.CHASE && _modeChangeTimer > _chaseModeTimer3)
                {
                    _modeChangeIteration = 4;
                    ChangeMode(Mode.SCATTER);
                    _modeChangeTimer = 0;
                }
            }
            else if (_modeChangeIteration == 4)
            {
                if (_currentMode == Mode.SCATTER && _modeChangeTimer > _scatterModeTimer4)
                {
                    ChangeMode(Mode.CHASE);
                    _modeChangeTimer = 0;
                }
            }
        }
        //-- Frightened Ghost Movement
        //-- Smart moves are disabled for a some moments
        else if (_currentMode == Mode.FRIGHTENED)
        {
            _frightenedModeTimer += Time.deltaTime;
            if(_frightenedModeTimer >= _frightenedModeDuration)
            {
                _backgroundAudio.clip = GetGameObject.Game.transform.GetComponent<GameBoard>()._backgroundAudioNormal;
                _backgroundAudio.Play();
                _frightenedModeTimer = 0;
                ChangeMode(_previousMode);
            }
            if(_frightenedModeTimer >= _startBlinkyAt)
            {
                _blinkTimer += Time.deltaTime;
                if(_blinkTimer >= 0.1f)
                {
                    _blinkTimer = 0f;
                    if (_frightenedModeIsWhite)
                    {
                        transform.GetComponent<Animator>().runtimeAnimatorController = _ghostScared;
                        _frightenedModeIsWhite = false;
                    }
                    else
                    {
                        transform.GetComponent<Animator>().runtimeAnimatorController = _ghostWhite;
                        _frightenedModeIsWhite = true;
                    }
                }
            }
        }
    }

    //-- Ghost Mode controller
    void ChangeMode (Mode mode)
    {
        if(_currentMode == Mode.FRIGHTENED)
        {
            _moveSpeed = _previousMoveSpeed;
        }
        if(mode == Mode.FRIGHTENED)
        {
            _previousMoveSpeed = _moveSpeed;
            _moveSpeed = _frightenedMoveSpeed;
        }
        if (_currentMode != mode)
        {
            _previousMode = _currentMode;
            _currentMode = mode;
        }

        UpdateAnimatorController();
    }

    //-- Activate Ghost Frightened Mode
    public void StartFrightenedMode()
    {
        if(_currentMode != Mode.CONSUMED)
        {
            _frightenedModeTimer = 0;
            _backgroundAudio.clip = GetGameObject.Game.transform.GetComponent<GameBoard>()._backgroundAudioFrightened;
            _backgroundAudio.Play();
            ChangeMode(Mode.FRIGHTENED);
        }
    }

    //-- Red Ghost "AI"
    Vector2 GetRedGhostTargetTile ()
    {
        Vector2 pacmanPosition = _pacman.transform.position;
        Vector2 targetTile = new Vector2(Mathf.RoundToInt(pacmanPosition.x), Mathf.RoundToInt(pacmanPosition.y));

        return targetTile;
    }


    //-- Pink Ghost "AI"
    Vector2 GetPinkyGhostTargetTile()
    {
        //-- Four tiles ahead Pacman
        //-- Taking account position and orientation

        Vector2 pacmanPosition = _pacman.transform.position;
        Vector2 pacmanOrientation = _pacman.GetComponent<Pacman>()._orientation;

        int pacmanX = Mathf.RoundToInt(pacmanPosition.x);
        int pacmanY = Mathf.RoundToInt(pacmanPosition.y);

        Vector2 pacmanTile = new Vector2(pacmanX, pacmanY);
        Vector2 targetTile = pacmanTile + (4 * pacmanOrientation);

        return targetTile;
    }

    //-- Blue Ghost "AI"
    Vector2 GetBlueGhostTargetTile()
    {
        //-- Select the position two tiles in front of Pacman
        //-- Draw vector from blinky to thar position
        //-- Double the lenght of the vector

        Vector2 pacmanPosition = _pacman.transform.position;
        Vector2 pacmanOrientation = _pacman.GetComponent<Pacman>()._orientation;

        int pacmanX = Mathf.RoundToInt(pacmanPosition.x);
        int pacmanY = Mathf.RoundToInt(pacmanPosition.y);

        Vector2 pacmanTile = new Vector2(pacmanX, pacmanY);
        Vector2 targetTile = pacmanTile + (2 * pacmanOrientation);

        //-- Tempory Blinky position
        Vector2 tempBlinkyPosition = GetGameObject.Blinky.transform.position;

        int blinkyX = Mathf.RoundToInt(tempBlinkyPosition.x);
        int blinkyY = Mathf.RoundToInt(tempBlinkyPosition.y);

        tempBlinkyPosition = new Vector2(blinkyX, blinkyY);

        float distance = Tools.GetDistance(tempBlinkyPosition, targetTile);
        distance *= 2;
        targetTile = new Vector2(tempBlinkyPosition.x + distance, tempBlinkyPosition.y + distance);

        return targetTile;
    }

    //-- Orange Ghost "AI"
    Vector2 GetOrangeGhostTargetTile()
    {
        //-- Calculate the distance from pacman
        //-- If the distance is greater the eight tiles targeting is the same as Blinky
        //-- If the distance is less than eight tiles, the target is his home node, som same as scatter mode

        Vector2 pacmanPosition = _pacman.transform.position;

        float distance = Tools.GetDistance(transform.localPosition, pacmanPosition);
        Vector2 targetTile = Vector2.zero;
        if (distance > 8)
        {
            targetTile = new Vector2(Mathf.RoundToInt(pacmanPosition.x), Mathf.RoundToInt(pacmanPosition.y)); 
        }
        else if (distance < 8)
        {
            targetTile = _homeNode.transform.position;
        }
        return targetTile;
    }

    //-- Ghost target tile controller
    Vector2 GetTargetTile()
    {
        if(ghostType == GhostType.RED)
        {
            return GetRedGhostTargetTile();
        }
        else if (ghostType == GhostType.PINK)
        {
            return GetPinkyGhostTargetTile();
        }
        else if (ghostType == GhostType.BLUE)
        {
            return GetBlueGhostTargetTile();
        }
        else if (ghostType == GhostType.ORANGE)
        {
            return GetOrangeGhostTargetTile();
        }
        return Vector2.zero;
    }

    void ReleasePinkGhost()
    {
        if(ghostType == GhostType.PINK && _isInGhostHouse)
        {
            _isInGhostHouse = false;
        }
    }

    void ReleaseBlueGhost()
    {
        if (ghostType == GhostType.BLUE && _isInGhostHouse)
        {
            _isInGhostHouse = false;
        }
    }

    void ReleaseOrangeGhost()
    {
        if (ghostType == GhostType.ORANGE && _isInGhostHouse)
        {
            _isInGhostHouse = false;
        }
    }

    //-- Release Ghost manager
    //-- Ghost in the box are released depending to the time they spend on the box
    void ReleaseGhosts()
    {
        _ghostReleaseTimer += Time.deltaTime;
        if (_ghostReleaseTimer > _pinkyReleaseTimer)
        {
            ReleasePinkGhost();
        }
        if (_ghostReleaseTimer > _inkyReleaseTimer)
        {
            ReleaseBlueGhost();
        }
        if (_ghostReleaseTimer > _clydeReleaseTimer)
        {
            ReleaseOrangeGhost();
        }
    }

    //-- Node research manager
    //-- Implement by the way fastest path algorithm to get to a target node
    Node ChooseNextNode()
    {
        //-- Node to target
        Vector2 targetTile = Vector2.zero;

        if(_currentMode == Mode.CHASE)
        {
            targetTile = GetTargetTile();
        }
        else if (_currentMode == Mode.SCATTER)
        {
            targetTile = _homeNode.transform.position;
        }
        else if (_currentMode == Mode.CONSUMED)
        {
            //-- Target House node if its consumed
            targetTile = _ghostHouse.transform.position;
        }

        Node nextNode = null;
        Node[] foundNodes = new Node[4];
        Vector2[] foundNodesDirection = new Vector2[4];

        int nodeCounter = 0;
        for(int i=0; i<_currentNode._neighbors.Length; i++)
        {
            if (_currentNode._enabledDirections[i] != _direction * -1)
            {
                //-- If ghost is consumed
                //-- Look for the fastest way to go back to the box
                //-- Movements don't depends anymore to the _current pacman node but to a home node
                if(_currentMode != Mode.CONSUMED)
                {
                    GameObject tile = GetGameObject.TileAtPosition(_currentNode.transform.position);
                    if(tile.transform.GetComponent<Tile>()._isGhostHouseEntrance == true)
                    {
                        //-- Found a ghost house don't want to allow a movement
                        if (_currentNode._enabledDirections[i] != Vector2.down)
                        {
                            foundNodes[nodeCounter] = _currentNode._neighbors[i];
                            foundNodesDirection[nodeCounter] = _currentNode._enabledDirections[i];
                            nodeCounter++;
                        }
                    }
                    else
                    {
                        foundNodes[nodeCounter] = _currentNode._neighbors[i];
                        foundNodesDirection[nodeCounter] = _currentNode._enabledDirections[i];
                        nodeCounter++;
                    }
                }
                //-- Else continue to hunt the pacman
                //-- Always by looking for the fastest way to get to the pacman node
                //-- Movements can be different for frightened mode
                else
                {
                    foundNodes[nodeCounter] = _currentNode._neighbors[i];
                    foundNodesDirection[nodeCounter] = _currentNode._enabledDirections[i];
                    nodeCounter++;
                }
            }
        }
        if (foundNodes.Length == 1)
        {
            nextNode = foundNodes[0];
            _direction = foundNodesDirection[0];
        }
        //-- Looking for the fastest way to go to the next node
        if (foundNodes.Length > 1)
        {
            float leastDistance = 100000f;
            for(int i=0; i<foundNodes.Length; i++)
            {
                if (foundNodesDirection[i] != Vector2.zero)
                {
                    float distance = Tools.GetDistance(foundNodes[i].transform.position, targetTile);
                    if (distance < leastDistance)
                    {
                        leastDistance = distance;
                        nextNode = foundNodes[i];
                        _direction = foundNodesDirection[i];
                    }
                }
            }
        }
        return nextNode;
    }
}
