﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GetGameObject
{
    public static GameObject Pacman = GameObject.Find("Pacman");
    public static GameObject PacmanByTag = GameObject.FindGameObjectWithTag("Pacman");
    public static GameObject Blinky = GameObject.Find("Blinky");
    public static GameObject Game = GameObject.Find("Game");

    public static GameObject [] Ghosts = GameObject.FindGameObjectsWithTag("Ghost");

    //-- Get Tile Object
    public static GameObject TileAtPosition(Vector2 position)
    {
        int tileX = Mathf.RoundToInt(position.x);
        int tileY = Mathf.RoundToInt(position.y);
        GameObject tile = Game.GetComponent<GameBoard>().board[tileX, tileY];
        if (tile != null)
        {
            return tile;
        }
        return null;
    }

    //-- Get Portal Object
    public static GameObject Portal(Vector2 position)
    {
        GameObject tile = Game.GetComponent<GameBoard>().board[(int)position.x, (int)position.y];
        if (tile != null)
        {
            if (tile.GetComponent<Tile>() != null)
            {
                if (tile.GetComponent<Tile>()._isPortal)
                {
                    GameObject otherPortal = tile.GetComponent<Tile>()._portalReceiver;
                    return otherPortal;
                }
            }
        }
        return null;
    }

}