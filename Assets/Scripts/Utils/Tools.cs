﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Tools
{
    //-- Set as param node.transform.position
    public static float LengthFromNode(Vector2 targetPosition, Vector2 previousPosition)
    {
        return (targetPosition - previousPosition).sqrMagnitude;
    }

    public static bool OverShotTarget(Vector2 localPosition, Node targetNode, Node previousNode)
    {
        float nodeToTarget = LengthFromNode(targetNode.transform.position, previousNode.transform.position);
        float nodeToSelf = LengthFromNode(localPosition, previousNode.transform.position);

        return nodeToSelf > nodeToTarget;
    }

    public static float GetDistance (Vector2 positionA, Vector2 positionB)
    {
        float distanceX = positionA.x - positionB.x;
        float distanceY = positionA.y - positionB.y;

        return Mathf.Sqrt(distanceX * distanceX + distanceY * distanceY);
    }

    public static Vector2 GetRandomTile()
    {
        int x = Random.Range(0, 28);
        int y = Random.Range(0, 36);

        return new Vector2(x, y);
    }
}
