﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GetNode
{

    //-- Get a node according it position
    public static Node NodeAtPosition(Vector2 position)
    {
        GameObject tile = GetGameObject.Game.GetComponent<GameBoard>().board[(int)position.x, (int)position.y];
        if (tile != null)
        {
            return tile.GetComponent<Node>();
        }
        return null;
    }
}
