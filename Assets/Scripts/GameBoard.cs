﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameBoard : MonoBehaviour
{
    private static readonly int _boardWidth = 28;
    private static readonly int _boardHeight = 36;

    private bool _isStartDeath = false;
    private bool _isStartConsumed = false;

    public bool _isPlayerOneUp = true;

    public int _totalPellets = 0;
    public int _score = 0;
    public int _playerOneScore = 0;
    public int _playerTwoScore = 0;
    public int _pacmanLives = 3;

    public AudioClip _backgroundAudioNormal;
    public AudioClip _backgroundAudioFrightened;
    public AudioClip _backgroundAudioPacmanDeath;
    public AudioClip _consumedGhostAudioClip;

    public Text _playerText;
    public Text _readyText;

    public Text _consumedGhostScoreText;

    public Text _highScoreText;
    public Text _playerOneUp;
    public Text _playerTwoUp;
    public Text _playerOneScoreText;
    public Text _playerTwoScoreText;
    public Image _playerLives2;
    public Image _playerLives3;


    public GameObject[,] board = new GameObject[_boardWidth, _boardHeight];

    // Start is called before the first frame update
    void Start()
    {
        Object[] objects = FindObjectsOfType(typeof(GameObject));
        foreach (GameObject gameObject in objects)
        {
            Vector2 position = gameObject.transform.position;
            if(gameObject.name != "Pacman" && gameObject.name != "Nodes" 
                && gameObject.name != "NonNodes" && gameObject.name != "Maze" 
                && gameObject.name != "Pellets" && gameObject.tag != "Ghost" 
                && gameObject.tag != "GhostHome" && gameObject.name != "Canvas"
                && gameObject.name != "PlayerText" && gameObject.name != "ReadyText"
                && gameObject.tag != "UIElements")
            {
                if (gameObject.GetComponent<Tile>() != null)
                {
                    if (gameObject.GetComponent<Tile>()._isPellet || gameObject.GetComponent<Tile>()._isSuperPellet)
                    {
                        _totalPellets++;
                    }
                }
                board[(int)position.x, (int)position.y] = gameObject;
            }
        }

        StartGame();
    }

    void Update()
    {
        UpdateUI();
    }

    void UpdateUI()
    {
        _playerOneScoreText.text = _playerOneScore.ToString();
        _playerTwoScoreText.text = _playerTwoScore.ToString();

        if(_pacmanLives == 3)
        {
            _playerLives3.enabled = true;
            _playerLives2.enabled = true;
        }
        else if(_pacmanLives == 2)
        {
            _playerLives3.enabled = true;
            _playerLives2.enabled = false;
        }
        else if (_pacmanLives == 1)
        {
            _playerLives3.enabled = false;
            _playerLives2.enabled = false;
        }
    }

    public void StartGame()
    {
        if (GameMenu._isOnePlayerGame)
        {
            _playerTwoUp.GetComponent<Text>().enabled = false;
            _playerTwoScoreText.GetComponent<Text>().enabled = false;
        }
        else
        {
            _playerTwoUp.GetComponent<Text>().enabled = true;
            _playerTwoScoreText.GetComponent<Text>().enabled = true;
        }

        if (_isPlayerOneUp)
        {
            StartCoroutine(StartBlinking(_playerOneUp));
        }
        else
        {
            StartCoroutine(StartBlinking(_playerTwoUp));
        }

        //-- Hide All Ghosts
        GameObject[] ghosts = GetGameObject.Ghosts;
        foreach (GameObject ghost in ghosts)
        {
            ghost.transform.GetComponent<SpriteRenderer>().enabled = false;
            ghost.transform.GetComponent<Ghost>()._canMove = false;
        }

        GameObject pacman = GetGameObject.Pacman;
        pacman.transform.GetComponent<SpriteRenderer>().enabled = false;
        pacman.transform.GetComponent<Pacman>()._canMove = false;

        StartCoroutine(ShowObjectAfter(2.25f));
    }

    public void StartConsumed(Ghost consumedGhost)
    {
        if (!_isStartConsumed)
        {
            _isStartConsumed = true;
            //-- Pause All the Ghosts
            GameObject[] ghosts = GetGameObject.Ghosts;
            foreach(GameObject ghost in ghosts)
            {
                ghost.transform.GetComponent<Ghost>()._canMove = false;
            }

            //-- Pause pacman
            GameObject pacman = GetGameObject.Pacman;
            pacman.transform.GetComponent<Pacman>()._canMove = false;

            //-- Hide pacman
            pacman.transform.GetComponent<SpriteRenderer>().enabled = false;

            //-- Hide the consumed ghost
            consumedGhost.transform.GetComponent<SpriteRenderer>().enabled = false;

            //-- Stop background Music
            transform.GetComponent<AudioSource>().Stop();

            Vector2 position = consumedGhost.transform.position;
            Vector2 viewPortPoint = Camera.main.WorldToViewportPoint(position);

            _consumedGhostScoreText.GetComponent<RectTransform>().anchorMin = viewPortPoint;
            _consumedGhostScoreText.GetComponent<RectTransform>().anchorMax = viewPortPoint;

            _consumedGhostScoreText.GetComponent<Text>().enabled = true;

            //-- Play the consumed sound
            transform.GetComponent<AudioSource>().PlayOneShot(_consumedGhostAudioClip);

            //-- Wait for the audio clip  to finish
            StartCoroutine(ProcessConsumedAfter(0.75f, consumedGhost));
        }
    }

    public void StartDeath ()
    {
      
        if(!_isStartDeath)
        {

            StopAllCoroutines();

            if (GameMenu._isOnePlayerGame)
            {
                _playerOneUp.GetComponent<Text>().enabled = true;
            }
            else
            {
                _playerOneUp.GetComponent<Text>().enabled = true;
                _playerTwoUp.GetComponent<Text>().enabled = true;
            }

            _isStartDeath = true;
            GameObject[] ghosts = GetGameObject.Ghosts;
            foreach (GameObject ghost in ghosts)
            {
                ghost.transform.GetComponent<Ghost>()._canMove = false;
            }
            GameObject pacman = GetGameObject.Pacman;
            pacman.transform.GetComponent<Pacman>()._canMove = false;
            pacman.transform.GetComponent<Animator>().enabled = false;

            transform.GetComponent<AudioSource>().Stop();
            StartCoroutine(ProcessDeathAfter(2));
        }
    }

    IEnumerator StartBlinking(Text blinkText)
    {
        yield return new WaitForSeconds(0.25f);

        blinkText.GetComponent<Text>().enabled = !blinkText.GetComponent<Text>().enabled;

        StartCoroutine(StartBlinking(blinkText));
    }

    IEnumerator ProcessConsumedAfter(float delay, Ghost consumedGhost)
    {
        yield return new WaitForSeconds(delay);

        //- Hide the score
        _consumedGhostScoreText.GetComponent<Text>().enabled = false;

        //-- Show Pacman
        GameObject pacman = GetGameObject.Pacman;
        pacman.transform.GetComponent<SpriteRenderer>().enabled = true;

        //-- Show Consumed Ghost
        consumedGhost.transform.GetComponent<SpriteRenderer>().enabled = true;

        //-- Resume All ghosts
        GameObject[] ghosts = GetGameObject.Ghosts;
        foreach (GameObject ghost in ghosts)
        {
            ghost.transform.GetComponent<Ghost>()._canMove = true;
        }

        //-- Resume pacman
        pacman.transform.GetComponent<Pacman>()._canMove = true;

        //-- Start background music
        transform.GetComponent<AudioSource>().Play();

        _isStartConsumed = false;
    }

    IEnumerator ShowObjectAfter(float delay)
    {
        yield return new WaitForSeconds(delay);
        GameObject[] ghosts = GetGameObject.Ghosts;
        foreach (GameObject ghost in ghosts)
        {
            ghost.transform.GetComponent<SpriteRenderer>().enabled = true;
        }
        GameObject pacman = GetGameObject.Pacman;
        pacman.transform.GetComponent<SpriteRenderer>().enabled = true;

        _playerText.transform.GetComponent<Text>().enabled = false;

        StartCoroutine(StartGameAfter(2));
    }

    IEnumerator StartGameAfter(float delay)
    {
        yield return new WaitForSeconds(delay);

        GameObject[] ghosts = GetGameObject.Ghosts;
        foreach (GameObject ghost in ghosts)
        {
            ghost.transform.GetComponent<Ghost>()._canMove = true;
        }
        GameObject pacman = GetGameObject.Pacman;
        pacman.transform.GetComponent<Pacman>()._canMove = true;

        _readyText.transform.GetComponent<Text>().enabled = false;

        transform.GetComponent<AudioSource>().clip = _backgroundAudioNormal;
        transform.GetComponent<AudioSource>().Play();
    }

    IEnumerator ProcessDeathAfter (float delay)
    {
        yield return new WaitForSeconds(delay);
        GameObject[] ghosts = GetGameObject.Ghosts;
        foreach (GameObject ghost in ghosts)
        {
            ghost.transform.GetComponent<SpriteRenderer>().enabled = false;
        }

        StartCoroutine(ProcessDeathAnimation(1.9f));
    }

    IEnumerator ProcessDeathAnimation(float delay)
    {
        GameObject pacman = GetGameObject.Pacman;
        pacman.transform.localScale = new Vector3(1, 1, 1);
        pacman.transform.localRotation = Quaternion.Euler(0, 0, 0);

        pacman.transform.GetComponent<Animator>().runtimeAnimatorController = pacman.transform.GetComponent<Pacman>()._deathAnimation;
        pacman.transform.GetComponent<Animator>().enabled = true;

        transform.GetComponent<AudioSource>().clip = _backgroundAudioPacmanDeath;
        transform.GetComponent<AudioSource>().Play();

        yield return new WaitForSeconds(delay);

        StartCoroutine(ProcessRestart(1));
    }

    IEnumerator ProcessRestart(float delay)
    {
        _pacmanLives -= 1;

        if (_pacmanLives == 0)
        {
            _playerText.transform.GetComponent<Text>().enabled = true;
            _readyText.transform.GetComponent<Text>().text = "GAME OVER";
            _readyText.transform.GetComponent<Text>().color = Color.red;

            _readyText.transform.GetComponent<Text>().enabled = true;

            GameObject pacman = GetGameObject.Pacman;
            pacman.transform.GetComponent<SpriteRenderer>().enabled = false;

            transform.GetComponent<AudioSource>().Stop();

            StartCoroutine(ProcessGameOver(2));
        }
        else
        {
            _playerText.transform.GetComponent<Text>().enabled = true;
            _readyText.transform.GetComponent<Text>().enabled = true;

            GameObject pacman = GetGameObject.Pacman;
            pacman.transform.GetComponent<SpriteRenderer>().enabled = false;

            transform.GetComponent<AudioSource>().Stop();

            yield return new WaitForSeconds(delay);

            StartCoroutine(ProcessRestartShowObjects(1));
        }
    }

    IEnumerator ProcessGameOver(float delay)
    {
        yield return new WaitForSeconds(delay);

        // SceneManager.LoadScene("GameMenu");
    }

    IEnumerator ProcessRestartShowObjects(float delay)
    {
        _playerText.transform.GetComponent<Text>().enabled = false;

        GameObject[] ghosts = GetGameObject.Ghosts;
        foreach (GameObject ghost in ghosts)
        {
            ghost.transform.GetComponent<SpriteRenderer>().enabled = true;
            ghost.transform.GetComponent<Ghost>().MoveToStartingPosition();
        }
        GameObject pacman = GetGameObject.Pacman;

        pacman.transform.GetComponent<Animator>().enabled = false;
        pacman.transform.GetComponent<SpriteRenderer>().enabled = true;
        pacman.transform.GetComponent<Pacman>().MoveToStartingPosition();

        yield return new WaitForSeconds(delay);

        Restart();
    }

    public void Restart()
    {
        _readyText.transform.GetComponent<Text>().enabled = false;

        GameObject pacman = GetGameObject.Pacman;
        pacman.transform.GetComponent<Pacman>().Restart();

        GameObject[] ghosts = GetGameObject.Ghosts;
        foreach (GameObject ghost in ghosts)
        {
            ghost.transform.GetComponent<Ghost>().Restart();
        }

        transform.GetComponent<AudioSource>().clip = _backgroundAudioNormal;
        transform.GetComponent<AudioSource>().Play();
        _isStartDeath = false;
    }

}
