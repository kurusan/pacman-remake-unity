﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    //-- Each tile on the maze have particularity
    //-- Each one get its role from these propreties

    //-- Portal Tile
    public bool _isPortal;

    //-- A pellet to consum
    public bool _isPellet;
    //-- A super pellet
    public bool _isSuperPellet;
    //-- A already consumed tile
    public bool _isConsumed;

    //-- A tile up to the ghost house box
    public bool _isGhostHouseEntrance;

    //-- A tile in the ghost house box
    public bool _isGhostHouse;

    //-- A portal tile which receive another one
    public GameObject _portalReceiver;

}
