﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{
    //-- Each node direct node neighbor
    public Node[] _neighbors;
    //-- Enabled direction from a node to her neighbor
    public Vector2[] _enabledDirections;

    //-- Start is called before the first frame update
    void Start()
    {
        InitEnabledDirection();
    }

    //-- Initialize each node direction to her nodes neighbors
    void InitEnabledDirection()
    {
        _enabledDirections = new Vector2[_neighbors.Length];
        for (int i = 0; i < _neighbors.Length; i++)
        {
            Node neighbor = _neighbors[i];
            Vector2 tempVector = neighbor.transform.localPosition - transform.localPosition;
            //-- Rounding nodes coordonates to get correct direction values
            _enabledDirections[i].x = Mathf.RoundToInt(tempVector.normalized.x);
            _enabledDirections[i].y = Mathf.RoundToInt(tempVector.normalized.y);
        }
    }

}
